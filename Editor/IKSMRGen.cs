﻿using System.Collections.Generic;
using System.Linq;
using Array = System.Array;
using Path = System.IO.Path;
using UnityEngine;
using UnityEditor;
using UnityEngine.Animations;
namespace ShaderIK {
public class IKSMRGen {
	struct ChainParam {
		public Vector3 localPosition;
		public Quaternion localRotation;
		public float[] length;
	}
	public static void GenMeshIKSMR(Mesh mesh, Transform[] bones, Transform[][] chains, Mesh mesh_, out Transform[] bones_, int quality=2, int ntarget=0) {
		if(ntarget == 0)
			ntarget = chains.Length;

		var armature = chains.SelectMany(c => c).ToArray();
		var boneToChain = chains.SelectMany((c,i) => Enumerable.Repeat(i, c.Length)).ToArray();
		var firstBone = chains.Select((c,i) => chains.Take(i).Sum(x => x.Length)).ToArray();
		var lastBone  = chains.Select((c,i) => chains.Take(i+1).Sum(x => x.Length)-1).ToArray();
		var parents   = Array.ConvertAll(armature, b => {
			if(b != null)
				do b = b.parent;
				while(b != null && Array.IndexOf(armature, b) < 0);
			return b == null ? 0 : Array.IndexOf(armature, b);
		});


		
		var frames = new Quaternion[armature.Length];
		var chainParams = new List<ChainParam>();
		for(int i=0; i<lastBone.Length; i++) {
			var first = firstBone[i];
			var last  = lastBone[i];
			if(i == 0)
				frames[first] = armature[first].rotation;  // has to be this because rootBone can't have bindpose
			else if(armature[first] == null) {
				// noop
			} else if (armature[last] == armature[last-1]) {
				for(int j=first; j<=last; j++)
					frames[j] = Quaternion.LookRotation(armature[first].forward, Vector3.up);
			} else {
				var bend = armature[first+1].position - (armature[first+0].position + armature[first+2].position)/2;
				var cosBend = Vector3.Dot(
					(armature[first+1].position-armature[first+0].position).normalized,
					(armature[first+2].position-armature[first+1].position).normalized);
				if(cosBend > 0.98f) {
					Debug.LogWarning($"chain[{i}] has small bend degree {Mathf.Acos(cosBend) * Mathf.Rad2Deg}. Did you forget to pose?");
					if(i == 1)
						bend = -Vector3.forward;
				}
				for(int j=first; j<last; j++)
					frames[j] = Quaternion.LookRotation(armature[j+1].position-armature[j].position, bend);
				frames[last] = frames[last-1];
			}

			var p = parents[first];
			// Debug.Log($"{i}: {first}~{last}");
			chainParams.Add(armature[first] ? new ChainParam{
				localRotation = Quaternion.Inverse(frames[p]) * frames[first],
				localPosition = Quaternion.Inverse(frames[p]) * armature[p].rotation
									* armature[p].InverseTransformPoint(armature[first].position),
				length = Enumerable.Range(first, last-first).Select(
						j => armature[j].InverseTransformPoint(armature[j+1].position).magnitude).ToArray(),
			} : new ChainParam());
		}
		var boneToFrame = frames.Select((f,i) =>
			armature[i] ? Quaternion.Inverse(f) * armature[i].rotation : Quaternion.identity).ToArray();

		// for(int i=0; i<lastBone.Length; i++)
		// 	Debug.Log($"chain{i}: {firstBone[i]}~{lastBone[i]}, parent={parents[firstBone[i]]}, "
		// 				+$"bone={frames[firstBone[i]]*new Vector3(0,0,1)}, bend={frames[firstBone[i]]*new Vector3(0,1,0)}");


		var bwBinds  = MeshUtil.MergeBoneWeightBindposes(mesh.boneWeights, mesh.bindposes, bones, armature, quality:quality, threshold:0.0323f);
		for(int v=0; v<mesh.vertexCount; v++)
			for(int i=0; i<quality; i++) {
				var bone = bwBinds[v, i].Key;
				var bind = bwBinds[v, i].Value;
				if(bind[3,3] != 0)
					bwBinds[v, i] = new KeyValuePair<int, Matrix4x4>(bone,
										Matrix4x4.Rotate(boneToFrame[bone]) * bind);
			}
		var vertices = mesh.vertices;
		var normals  = mesh.normals;
		var tangents = mesh.tangents;
		var uv0 = new List<Vector4>();
		mesh.GetUVs(0, uv0);

		var chainSets = new int[mesh.vertexCount][];
		for(int v=0; v<mesh.vertexCount; v++) {
			var cs = new SortedSet<int>();
			for(int i=0; i<quality; i++)
				if(bwBinds[v, i].Value[3,3] > 0)
					for(var bone = bwBinds[v, i].Key; bone >= 0;) {
						var c = boneToChain[bone];
						cs.Add(boneToChain[bone]);
						if(bone == lastBone[c] && c < ntarget)
							break;
						bone = parents[firstBone[c]];
					}
			chainSets[v] = cs.ToArray();
			if(cs.Contains(6))
				Debug.Log($"chainSets[v]={string.Join(",", chainSets[v].Select(x=>$"{x}"))}");
		}
		// return;

		var vertices_ = new List<Vector3>();
		var normals_  = new List<Vector3>();
		var tangents_ = new List<Vector4>();
		var boneWeights_ = new List<BoneWeight>();
		var uvs_v = Array.ConvertAll(new int[quality], x => new List<Vector4>());
		var uvs_n = Array.ConvertAll(new int[quality], x => new List<Vector4>());
		var uvs_b = Array.ConvertAll(new int[2], x => new List<Vector4>());

		System.Func<IEnumerable<int>, int[]> chainSetToBoneSet = (IEnumerable<int> cs) => {
			var bs = new List<int>();
			foreach(var c in cs)
				if(bs.Count == 0)
					bs.Add(lastBone[c]);
				else
					for(int b=firstBone[c]; b<=lastBone[c]; b++)
						bs.Add(b);
			Debug.Assert(bs.Count%3==1);
			return bs.ToArray();
		};
		System.Action<int, int[]> emitChainData = (int c, int[] boneSet) => {
			var pbone = parents[firstBone[c]];
			var idx = System.Math.Max(0, Array.IndexOf(boneSet, pbone));

			vertices_.Add(new Vector3(0,0,0));
			normals_. Add(new Vector3(1,0,0));
			tangents_.Add(new Vector4(0,1,0, (c<ntarget?c:chains.Length+c) + idx/8f));
			boneWeights_.Add(new BoneWeight{boneIndex0=(c<ntarget?c:boneToChain[pbone]), weight0=1});

			var pos = chainParams[c].localPosition;
			var rot = chainParams[c].localRotation.eulerAngles * Mathf.Deg2Rad;
			var len = chainParams[c].length;
			uvs_b[0].Add(new Vector4(pos.x, pos.y, pos.z, 0 < len.Length ? len[0] : 0));
			uvs_b[1].Add(new Vector4(rot.x, rot.y, rot.z, 1 < len.Length ? len[1] : 0));
		};
		System.Action<int, int[]> emitVertData = (int v, int[] boneSet) => {
			for(int i=0; i<quality; i++) {
				var bone = bwBinds[v, i].Key;
				var bind = bwBinds[v, i].Value;
				var weight = bind[3,3];
				var vertex = bind.MultiplyPoint3x4(vertices[v]);
				var normal = bind.MultiplyVector  (normals[v]);

				var idx = Array.IndexOf(boneSet, bone);
				if(weight == 0) {
					idx = 0;
					vertex = normal = Vector3.zero;
				} else if(idx < 0) {
					for(var b = bone; idx < 0; idx = Array.IndexOf(boneSet, b))
						b = parents[b];
					idx = System.Math.Max(idx, 0);
					Debug.LogWarning($@"vertex is skinned with bone not in triangle boneSet {{{
							string.Join(",", Array.ConvertAll(boneSet, b=>armature[b].name))
						}}}: retarget {armature[bone].name} => {armature[boneSet[idx]].name}");
					
					var q0 = boneToFrame[boneSet[idx]];
					var q1 = Quaternion.Inverse(boneToFrame[bone]);
					var b2b = armature[boneSet[idx]].worldToLocalMatrix * armature[bone].localToWorldMatrix;
					vertex = q0 * b2b.MultiplyPoint3x4(q1 * vertex / weight) * weight;
					normal = q0 * b2b.MultiplyVector  (q1 * normal);
				}
				uvs_v[i].Add(new Vector4(vertex.x, vertex.y, vertex.z, weight/2 + idx));
				uvs_n[i].Add(new Vector4(normal.x, normal.y, normal.z, uv0[v][i]));
			}
		};
		System.Func<int, bool> emitRawData = (int v) => {
			var bone = bwBinds[v, 0].Key;
			var bind = bwBinds[v, 0].Value;
			var weight = bind[3,3];
			var vertex = bind.MultiplyPoint3x4(vertices[v]);
			var normal = bind.MultiplyVector  (normals[v]);
			if(!(Mathf.Abs(weight-1)<1e-5 && bone == lastBone[boneToChain[bone]] && boneToChain[bone] < ntarget))
				return false;

			vertices_.Add(vertex);
			normals_.Add(normal);
			tangents_.Add(new Vector4(0,0,0,-1));
			boneWeights_.Add(new BoneWeight{boneIndex0=boneToChain[bone], weight0=1});

			uvs_b[0].Add(new Vector4(uv0[v].x, uv0[v].y, 0, 0)); // fallback uv when shader is off
			uvs_b[1].Add(Vector4.zero);
			for(int i=0; i<quality; i++) {
				uvs_v[i].Add(Vector4.zero);
				uvs_n[i].Add(new Vector4(0, 0, 0, uv0[v][i]));
			}
			return true;
		};

		var vpos = Enumerable.Repeat(-1, mesh.vertexCount).ToArray();
		for(int v=0; v<mesh.vertexCount; v++) {
			var chainSet = chainSets[v];
			if(chainSet.Length <= (chainSet[0] == 0 ? 2 : 1)) {
				vpos[v] = vertices_.Count;
				if(!emitRawData(v)) {
					var boneSet = chainSetToBoneSet(chainSet[0] != 0 ? new[]{0, chainSet[0]} : chainSet);
					emitVertData(v, boneSet);
					emitChainData(chainSet[chainSet.Length-1], boneSet);
				}
			}
		}
		var nvert0 = vertices_.Count;

		if(mesh.subMeshCount > 1)
			Debug.LogWarning($"mesh has >1 submeshes: combined");

		var triangles = mesh.triangles;
		var triangles_ = Array.ConvertAll(new int[2], x => new List<int>());
		for(int i=0; i<triangles.Length; i+=3)
			if(Enumerable.Range(0, 3).All(j => vpos[triangles[i+j]] >= 0))
				for(int j=0; j<3; j++)
					triangles_[0].Add(vpos[triangles[i+j]]);
			else {
				var cs = new SortedSet<int>(Enumerable.Range(0, 3).SelectMany(j => chainSets[triangles[i+j]]));
				if(cs.Count > 3)
					Debug.LogWarning($@"triangle is skinned with >3 chains {{{
						string.Join(", ", cs.Select(c=>armature[lastBone[c]].name))}}}: truncated");
				
				var boneSet = chainSetToBoneSet(cs.Take(3));
				for(int j=0; j<3; j++) {
					triangles_[1].Add(vertices_.Count);
					var v = triangles[i+j];
					emitVertData(v, boneSet);
					emitChainData(boneToChain[boneSet[System.Math.Min(j*3, boneSet.Length-1)]], boneSet);
				}
			}
		var nvert1 = vertices_.Count - nvert0;

		mesh_.Clear();
		mesh_.ClearBlendShapes();
		mesh_.subMeshCount = triangles_.Length;
		mesh_.indexFormat = nvert0 < 65536 && nvert1 < 65536 ? UnityEngine.Rendering.IndexFormat.UInt16
															: UnityEngine.Rendering.IndexFormat.UInt32;
		mesh_.SetVertices(vertices_);
		mesh_.SetNormals (normals_ );
		mesh_.SetTangents(tangents_);
		mesh_.boneWeights = boneWeights_.ToArray();
		mesh_.bindposes = lastBone.Take(ntarget).Select(b => Matrix4x4.Rotate(Quaternion.Inverse(boneToFrame[b]))).ToArray();
		bones_          = lastBone.Take(ntarget).Select(b => armature[b]).ToArray();

		mesh_.SetIndices(triangles_[0].ToArray(), MeshTopology.Triangles, 0, false);
		mesh_.SetIndices(triangles_[1].Select(x=>x-nvert0).ToArray(), MeshTopology.Triangles, 1, false, nvert0);
		mesh_.RecalculateBounds();

		Debug.Log($"#vertices=({nvert0}, {nvert1}), #triangles=({triangles_[0].Count/3}, {triangles_[1].Count/3})");

		for(int i=0; i<2; i++)
			mesh_.SetUVs(i, uvs_b[i]);
		for(int i=0; i<quality; i++) {
			mesh_.SetUVs(i*2+2, uvs_v[i]);
			mesh_.SetUVs(i*2+3, uvs_n[i]);
		}


		var delta  = new Vector3[mesh.vertexCount];
		var delta_ = new Vector3[vertices_.Count];
		for(int bi=0; bi<mesh.blendShapeCount; bi++) {
			var name = mesh.GetBlendShapeName(bi);
			for(int fi=0; fi<mesh.GetBlendShapeFrameCount(bi); fi++) {
				var weight = mesh.GetBlendShapeFrameWeight(bi, fi);
				mesh.GetBlendShapeFrameVertices(bi, fi, delta, null, null);

				Array.Clear(delta_, 0, delta_.Length);
				for(int v=0; v<mesh.vertexCount; v++)
					if(delta[v] != Vector3.zero) {
						var v2 = vpos[v];
						if(v2 < 0) {
							Debug.LogWarning($"blendShape[\"{name}\"] moves vert skinned with >1 chains: skipped");
							continue;
						}
						for(int i=0; i<quality; i++) {
							var bone = bwBinds[v, i].Key;
							var bind = bwBinds[v, i].Value;
							if(bind[3,3] == 0)
								break;

							var dv = bind.MultiplyVector(delta[v]);
							var c = Mathf.FloorToInt(tangents_[v2].w);
							var bone2 = c >= 0 ? lastBone[c] : bone;
							if(bone2 != bone) {
								var frameToFrame = Quaternion.Inverse(frames[bone2]) * frames[bone];
								dv = frameToFrame * dv;
								Debug.LogWarning($"blendShape[\"{name}\"] moves vert with non-target bone: "
									+ $"retarget {armature[bone].name} => {armature[bone2].name}");
							}
							delta_[v2] += dv;
						}
					}
				mesh_.AddBlendShapeFrame(name, weight, delta_, null, null);
			}
		}
	}
	static void FindDescendants(Transform root, Dictionary<string, Transform> results) {
		var q = new Queue<Transform>();
		q.Enqueue(root);
		while (q.Count > 0) {
			var c = q.Dequeue();
			if(results.ContainsKey(c.name))
				results[c.name] = c;
			foreach(Transform t in c)
				q.Enqueue(t);
		}
	}
	static HumanBodyBones[] LimbFromEnd(HumanBodyBones bone2) {
		var bone1 = (HumanBodyBones)HumanTrait.GetParentBone((int)bone2);
		var bone0 = (HumanBodyBones)HumanTrait.GetParentBone((int)bone1);
		return new[]{bone0, bone1, bone2};
	}
	[MenuItem("ShaderIK/Generate IKSMR Mesh")]
	static void GenIKSMRMesh() {
		var smr = Selection.activeGameObject.GetComponent<SkinnedMeshRenderer>();
		var animator = smr.gameObject.GetComponentInParent<Animator>();
		Debug.Assert(smr && animator && animator.isHuman, "expect a skinnedMeshRenderer inside a humanoid animator");

		var humanChains = new List<HumanBodyBones[]>(new HumanBodyBones[][]{
			new HumanBodyBones[]{HumanBodyBones.Hips},
			// Spine-Chest-Head has wrong bend direction, dont use it
			// Chest-Neck-Head sometimes bends chest too much
			// Spine-Neck-Head looks best out of Spine-Chest-Neck-Head, but it reduces reachable space
			new HumanBodyBones[]{HumanBodyBones.Spine, HumanBodyBones.Neck, HumanBodyBones.Head},
			LimbFromEnd(HumanBodyBones.LeftHand), LimbFromEnd(HumanBodyBones.RightHand),
			LimbFromEnd(HumanBodyBones.LeftFoot), LimbFromEnd(HumanBodyBones.RightFoot),
			new HumanBodyBones[]{HumanBodyBones.LeftEye,  HumanBodyBones.Jaw, HumanBodyBones.Jaw},
			new HumanBodyBones[]{HumanBodyBones.RightEye, HumanBodyBones.Jaw, HumanBodyBones.Jaw},
			LimbFromEnd(HumanBodyBones.LeftThumbDistal),  LimbFromEnd(HumanBodyBones.RightThumbDistal),
			LimbFromEnd(HumanBodyBones.LeftIndexDistal),  LimbFromEnd(HumanBodyBones.RightIndexDistal),
			LimbFromEnd(HumanBodyBones.LeftMiddleDistal), LimbFromEnd(HumanBodyBones.RightMiddleDistal),
			LimbFromEnd(HumanBodyBones.LeftRingDistal),   LimbFromEnd(HumanBodyBones.RightRingDistal),
			LimbFromEnd(HumanBodyBones.LeftLittleDistal), LimbFromEnd(HumanBodyBones.RightLittleDistal),

		});
		var eyeTarget = animator.transform.Find("LookAt");
		// Debug.Log($"eyeTarget={eyeTarget}");
		var chains = humanChains.ConvertAll(c => Array.ConvertAll(c, b =>
			b == HumanBodyBones.Jaw ? eyeTarget : animator.GetBoneTransform(b)));

		var mesh = smr.sharedMesh;
		var path = AssetDatabase.GetAssetPath(mesh);
		path = Path.Combine(Path.GetDirectoryName(path), $"{mesh.name}IK.asset");

		var smr_t = smr.transform.parent.Find($"{smr.name}IK");
		if(smr_t)
			path = AssetDatabase.GetAssetPath(smr_t.GetComponent<SkinnedMeshRenderer>().sharedMesh);
		Debug.Log($"save at {path}");

		var mesh_ = (Mesh)AssetDatabase.LoadMainAssetAtPath(path);
		if(mesh_ == null) {
			mesh_ = new Mesh();
			AssetDatabase.CreateAsset(mesh_, path);
		}

		Transform[] bones_;

		AvatarPoser.SetSkeletonPose(animator);
		AvatarPoser.RelaxMuscle(animator);
		GenMeshIKSMR(smr.sharedMesh, smr.bones, chains.ToArray(), mesh_, out bones_, quality:2, ntarget:6);
		AssetDatabase.SaveAssets();

		var smr_ = Object.Instantiate(smr, smr.transform.parent);
		smr_.name = $"{smr.name}IK";

		smr_.sharedMesh = mesh_;
		smr_.sharedMaterials = new Material[2]{Resources.Load<Material>("IKSMR1"), Resources.Load<Material>("IKSMR3")};
		smr_.bones = bones_;
		smr_.rootBone = bones_[0];
	}
}
}