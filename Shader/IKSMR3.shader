Shader "IK/IKSMR3" {
Properties {
	[Header(Texture)]
	[NoScaleOffset]
	_MainTex ("Albedo", 2D) = "white" {}
	_Color ("Color", Color) = (1,1,1,1)

	[Header(Clipping)]
	[Enum(UnityEngine.Rendering.CullMode)] _Cull("Face Culling", Float) = 2
	[Toggle(_ALPHATEST_ON)] _AlphaTest("Alpha Test", Float) = 0
	_Cutoff ("Alpha Cutoff", Range(0, 1)) = 0
	_NearClip ("NearClip", Float) = 0

	[Header(Motion Distribution (Bend.Swing.Twist))]
	_Spread00 ("Spine", 	Vector) = (40, 40, 40, 1)
	_Spread01 ("Head",		Vector) = (40, 40, 80, 1)
	_Spread10 ("UpperArm",	Vector)	= (90, 90, 45, 1)
	_Spread11 ("Hand",		Vector) = (30, 60,135, 1)
	_Spread20 ("UpperLeg",	Vector) = (60, 60, 60, 1)
	_Spread21 ("Foot",		Vector) = (30, 15, 30, 1)
	_Spread30 ("Eye",		Vector) = (60, 60,  9, 0)
	_Spread40 ("ThumbProximal",	Vector) = (20, 25,  9, 1)
	_Spread41 ("ThumbDistal",	Vector) = (40,  9,  9, 1)
	_Spread50 ("IndexProximal",	Vector) = (45, 20,  9, 1)
	_Spread51 ("IndexDistal",	Vector) = (45,  9,  9, 1)

	[Header(Finger Muscle (Stretch3.Spread))]
	_Finger00 ("LeftThumb",		Vector) = (0, 0, 0, 0)
	_Finger01 ("RightThumb",	Vector) = (0, 0, 0, 0)
	_Finger10 ("LeftIndex",		Vector) = (0, 0, 0, 0)
	_Finger11 ("RightIndex",	Vector) = (0, 0, 0, 0)
	_Finger20 ("LeftMiddle",	Vector) = (0, 0, 0, 0)
	_Finger21 ("RightMiddle",	Vector) = (0, 0, 0, 0)
	_Finger30 ("LeftRing",		Vector) = (0, 0, 0, 0)
	_Finger31 ("RightRing",		Vector) = (0, 0, 0, 0)
	_Finger40 ("LeftLittle",	Vector) = (0, 0, 0, 0)
	_Finger41 ("RightLittle",	Vector) = (0, 0, 0, 0)
}
SubShader {
	Tags { "Queue"="Geometry" "RenderType"="Opaque" }
	Pass {
		Tags { "LightMode"="ForwardBase" }
		Cull [_Cull]
		CGPROGRAM
		#pragma target 5.0
		#pragma exclude_renderers gles
		#pragma vertex vert
		#pragma fragment frag
		#pragma geometry geom
		#pragma shader_feature _ALPHATEST_ON
		#define FINGER_MUSCLE
		#define EYE_TRACKING
		#include <UnityCG.cginc>
		#include <Lighting.cginc>
		#include "IKSMR.hlsl"
		#include "Frag.hlsl"

		// static bool fingerIK = false;
		

		void vert(VertInputIKSMR i, out GeomInputIKSMR o) {
			o = i;
		}
		[maxvertexcount(3)]
		void geom(triangle GeomInputIKSMR i[3], inout TriangleStream<FragInput> stream) {
			UNITY_SETUP_INSTANCE_ID(i[0]);
			packed3x4 mats[7] = (packed3x4[7])0;
			pack3x4(mats[0], ((VertInputIKSMR)i[0]).GetLimbTarget());
			{for(uint I=1; I<3 && ((VertInputIKSMR)i[I]).GetLimbRawId() != 0; I++) {
				float3x4 matP = unpack3x4(mats[((VertInputIKSMR)i[I]).GetLimbParent()]);
				float3x4 mat[3];
				solveIK(i[I], matP, mat[0], mat[1], mat[2]);
				pack3x4(mats[I*3-2], mat[0]);
				pack3x4(mats[I*3-1], mat[1]);
				pack3x4(mats[I*3-0], mat[2]);
			}}
			{for(uint I=0; I<3; I++) {
				float3 vertex = 0, normal = 0;
				UNITY_UNROLL for(uint J=0; J<QUALITY; J++) {
					uint bone; float4 vtx; float3 nml;
					((VertInputIKSMR)i[I]).GetSkin(J, bone, vtx, nml);
					float3x4 mat = unpack3x4(mats[bone]);
					vertex += mul(mat, vtx);
					normal += mul(mat, nml);
				}
				FragInput o;
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				o.vertex = mul(unity_ObjectToWorld, float4(vertex, 1));
				o.normal = mul(unity_ObjectToWorld, float4(normal, 0));
				o.pos = UnityWorldToClipPos(o.vertex);
				o.tex =  refSpread()?((VertInputIKSMR)i[I]).GetUV():0;
				stream.Append(o);
			}}
		}
		ENDCG
	}
}
}