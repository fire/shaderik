cbuffer SpreadBuffer {
	float4 _Spread[6][2] : packoffset(c0);  
	float4 _Spread00 : packoffset(c0);
	float4 _Spread01 : packoffset(c1);
	float4 _Spread10 : packoffset(c2);
	float4 _Spread11 : packoffset(c3);
	float4 _Spread20 : packoffset(c4);
	float4 _Spread21 : packoffset(c5);
	float4 _Spread30 : packoffset(c6);
	float4 _Spread31 : packoffset(c7);
	float4 _Spread40 : packoffset(c8);
	float4 _Spread41 : packoffset(c9);
	float4 _Spread50 : packoffset(c10);
	float4 _Spread51 : packoffset(c11);
};
#ifdef FINGER_MUSCLE
cbuffer FingerBuffer {
	float4 _Finger[10] : packoffset(c0);  
	float4 _Finger00 : packoffset(c0);
	float4 _Finger01 : packoffset(c1);
	float4 _Finger10 : packoffset(c2);
	float4 _Finger11 : packoffset(c3);
	float4 _Finger20 : packoffset(c4);
	float4 _Finger21 : packoffset(c5);
	float4 _Finger30 : packoffset(c6);
	float4 _Finger31 : packoffset(c7);
	float4 _Finger40 : packoffset(c8);
	float4 _Finger41 : packoffset(c9);
}
#endif
bool refSpread() {
	UNITY_BRANCH
	if(asuint(unity_ObjectToWorld[3][3]) == 0)
		return (
			+_Spread00+_Spread01
			+_Spread10+_Spread11
			+_Spread20+_Spread21
			+_Spread30+_Spread31
			+_Spread40+_Spread41
			+_Spread50+_Spread51
			#ifdef FINGER_MUSCLE
			+_Finger00+_Finger01
			+_Finger10+_Finger11
			+_Finger20+_Finger21
			+_Finger30+_Finger31
			+_Finger40+_Finger41
			#endif
		).x != 0;
	else
		return true;
}

#include "LimbIK.hlsl"

float3x3 mulEulerYXZ(float3x3 m, float3 rad) {
	float3x3 m0;
	float3 c, s;
	sincos(rad, s, c);
	m0 = m;
	m.c2 = m0.c2*c.y + m0.c0*s.y;
	m.c0 = m0.c0*c.y - m0.c2*s.y;
	m0 = m;
	m.c1 = m0.c1*c.x + m0.c2*s.x;
	m.c2 = m0.c2*c.x - m0.c1*s.x;
	m0 = m;
	m.c0 = m0.c0*c.z + m0.c1*s.z;
	m.c1 = m0.c1*c.z - m0.c0*s.z;
	return m;
}
void solveLimbIK(float2 len, float3x4 mat0, float3x4 matT, float3 wt0, float4 wt2, out float3x3 rot0, out float3x3 rot1) {
	// compute relative transform
	matT.c3 -= mat0.c3;
	matT = mul(transpose((float3x3)mat0), matT);
	matT.c3 /= dot(mat0.c0, mat0.c0); // remove bone0 rescaling
	// remove scale
	float rsq = rsqrt(dot(matT.c0, matT.c0));
	matT.c0 *= rsq;
	matT.c1 *= rsq;
	matT.c2  = cross(matT.c0, matT.c1); // skip matT.c2 to save work
	// stretch bone to avoid detached joints
	len *= max(1,length(matT.c3)/(len[0]+len[1])); 
	float3x3 rot2;
	solveLimbIK(len, matT.c3, (float3x3)matT, wt0, wt2, rot0, rot1, rot2);
}

#define QUALITY 2
struct GeomInputIKSMR {
	float3 vertex  : TEXCOORD0;
	float3 normal  : TEXCOORD1;
	float4 tangent : TEXCOORD2;
	float4 uvBone[2] : TEXCOORD3;
	float4 uvSkin[QUALITY][2] : TEXCOORD5;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};
struct VertInputIKSMR {
	float3 vertex  : POSITION;
	float3 normal  : NORMAL;
	float4 tangent : TANGENT;
	float4 uvBone[2] : TEXCOORD0;
	float4 uvSkin[QUALITY][2] : TEXCOORD2;
	UNITY_VERTEX_INPUT_INSTANCE_ID

	float GetLimbRawId() {
		return tangent.w;
	}
	uint GetLimbId() {
		return floor(GetLimbRawId());
	}
	uint GetLimbParent() {
		return round(frac(GetLimbRawId())*8);
	}
	float2 GetLimbLength() {
		return float2(uvBone[0].w, uvBone[1].w);
	}
	float3x4 GetLimbRoot(float3x4 matP) {
		float3x3 rot0 = mulEulerYXZ((float3x3)matP, uvBone[1].xyz);
		float3x4 mat0;
		mat0.c0 = rot0.c0;
		mat0.c1 = rot0.c1;
		mat0.c2 = rot0.c2;
		mat0.c3 = mul(matP, float4(uvBone[0].xyz, 1));
		return mat0;
	}
	float3x4 GetLimbTarget() {
		float3x4 matT;
		matT.c0 = normal;
		matT.c1 = tangent.xyz;
		matT.c2 = cross(normalize(matT.c0), matT.c1);
		matT.c3 = vertex;
		return matT;
	}
	void GetSkin(uint idx, out uint bone, out float4 vertex, out float3 normal) {
		bone = floor(uvSkin[idx][0].w);
		vertex = float4(uvSkin[idx][0].xyz, frac(uvSkin[idx][0].w)*2);
		normal = uvSkin[idx][1].xyz;
	}
	float2 GetUV() {
		return float2(uvSkin[0][1].w, uvSkin[1][1].w);
	}
};

float4 _Fingers;
void solveIK(VertInputIKSMR i, float3x4 matP, out float3x4 mat0, out float3x4 mat1, out float3x4 mat2) {
	uint limb = i.GetLimbId();
	float4 spread[2] = _Spread[min(5,limb/2-(limb>=18?9:0))];
	float2 len = i.GetLimbLength();

	float3x3 rot0, rot1;
	mat0 = i.GetLimbRoot(matP);
#ifdef FINGER_MUSCLE
	float4 co = 0, si = 0;
	if(limb >= 26) {
		float4 muscle = _Finger[limb-26];
		float4 degree = muscle * float4(spread[0].x, spread[1].xx, spread[0].y) - float4(0,spread[0].x,0,0);
		sincos(3.14159265359/180 * degree, si, co);

		rot0 = float3x3(1,0,0, 0,co[0],+si[0], 0,-si[0],co[0]);
		rot1 = float3x3(1,0,0, 0,co[1],+si[1], 0,-si[1],co[1]);

		rot0 = mul(float3x3(
			co[3],0,-si[3],
			0,1,0,
			+si[3],0,co[3]), rot0);
	} else
#endif
	{
		float3x4 matT = i.GetLimbTarget();
#ifdef EYE_TRACKING
		if(limb/2 == 12)
			matT.c3 = mul(unity_WorldToObject, float4(_WorldSpaceCameraPos, 1));
#endif
		solveLimbIK(len, mat0, matT, rcp(spread[0].xyz), spread[0].w == 0 ? 0 : rcp(spread[1]), rot0, rot1);
	}

	float3x3 r = mul((float3x3)mat0, rot0);
	mat0.c0 = r.c0;
	mat0.c1 = r.c1;
	mat0.c2 = r.c2;

	r = mul(r, rot1);
	mat1.c0 = r.c0;
	mat1.c1 = r.c1;
	mat1.c2 = r.c2;
	mat1.c3 = mat0.c3 + mat0.c2 * len[0];

#ifdef FINGER_MUSCLE
	if(limb >= 26) {
		float3x3 rot2 = float3x3(1,0,0, 0,co[2],+si[2], 0,-si[2],co[2]);
		r = mul(r, rot2);
		mat2.c0 = r.c0;
		mat2.c1 = r.c1;
		mat2.c2 = r.c2;
		mat2.c3 = mat1.c3 + mat1.c2 * len[1];
	} else
#endif
		mat2 = i.GetLimbTarget();
}

#if defined(_DETAIL_MULX2) && SHADER_TARGET > 40
	struct packed3x4 {
		uint3 rot;
		float3 pos;
	};
	float3x4 unpack3x4(packed3x4 pack) {
		float3x4 m;
		m.c0 = f16tof32((pack.rot));
		m.c1 = f16tof32((pack.rot >> 16));
		m.c2 = cross(normalize(m.c0), m.c1);
		m.c3 = pack.pos;
		return m;
	}
	void pack3x4(out packed3x4 pack, float3x4 m) {
		pack.rot = f32tof16(m.c0) + (f32tof16(m.c1) << 16);
		pack.pos = m.c3;
	}
#else
	typedef float3 packed3x4[3];
	float3x4 unpack3x4(packed3x4 pack) {
		float3x4 m;
		m.c0 = pack[0];
		m.c1 = pack[1];
		m.c2 = cross(normalize(m.c0), m.c1);
		m.c3 = pack[2];
		return m;
	}
	void pack3x4(out packed3x4 pack, float3x4 m) {
		pack[0] = m.c0;
		pack[1] = m.c1;
		pack[2] = m.c3;
	}
#endif