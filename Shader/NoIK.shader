Shader "IK/NoIK" {
Properties {
	_Color ("Color", Color) = (1,1,1,1)
	_MainTex ("MainTex", 2D) = "white" {}
	[Enum(UnityEngine.Rendering.CullMode)] _Cull("Cull", Float) = 2
	[Toggle(_ALPHATEST_ON)] _AlphaTest("AlphaTest", Float) = 0
	_Cutoff ("Cutoff", Range(0, 1)) = 0
	_NearClip ("NearClip", Float) = 0
}
SubShader {
	Tags { "Queue"="Geometry" "RenderType"="Opaque" }
	Pass {
		Tags { "LightMode"="ForwardBase" }
		Cull [_Cull]
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma shader_feature _ALPHATEST_ON
		#include <UnityCG.cginc>
		#include <Lighting.cginc>
		#include "Frag.hlsl"

		struct VertInput {
			float3 vertex  : POSITION;
			float3 normal  : NORMAL;
			float2 texcoord : TEXCOORD0;
			UNITY_VERTEX_INPUT_INSTANCE_ID
		};
		void vert(VertInput i, out FragInput o) {
			float3 vertex = i.vertex;
			float3 normal = i.normal;
			UNITY_SETUP_INSTANCE_ID(i);
			UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
			o.vertex = mul(unity_ObjectToWorld, float4(vertex, 1));
			o.normal = mul(unity_ObjectToWorld, float4(normal, 0));
			o.tex = i.texcoord;
			o.pos = UnityWorldToClipPos(o.vertex);
		}
		ENDCG
	}
}
}