#define c0 _11_21_31
#define c1 _12_22_32
#define c2 _13_23_33
#define c3 _14_24_34
#define out(foo) (foo)
////
static float3x3 axisAngleRotate(float3 axis, float2 cplx, float3x3 v) {
	float3 si_axis = cplx.y * axis;
	float3 cc_axis = axis - cplx.x*axis; // Rodrigues rotation formula
	v.c0 = cplx.x * v.c0 + (dot(axis, v.c0) * cc_axis + cross(si_axis, v.c0)); // MAD optimization
	v.c1 = cplx.x * v.c1 + (dot(axis, v.c1) * cc_axis + cross(si_axis, v.c1));
	v.c2 = cplx.x * v.c2 + (dot(axis, v.c2) * cc_axis + cross(si_axis, v.c2));
	return v;
}
static float3x3 fromToRotate(float3 src, float3 dst, float3x3 v) {
	float  co = dot(src, dst);
	float3 si_axis = cross(src, dst);
	float3 rc_axis = si_axis / (1+co);
	v.c0 = co * v.c0 + (dot(si_axis, v.c0) * rc_axis + cross(si_axis, v.c0));
	v.c1 = co * v.c1 + (dot(si_axis, v.c1) * rc_axis + cross(si_axis, v.c1));
	v.c2 = co * v.c2 + (dot(si_axis, v.c2) * rc_axis + cross(si_axis, v.c2));
	return v;
}
// gradient at origin of the rational function dot(wt, 1-2*{x^2, y^2, z^2/(1-x^2-y^2)}) in cos(angle), sin(angle)
// where rotation(axis, angle) * rot == quaternion(x,y,z,w)
// note 1-2*z^2/(1-x^2-y^2)} == 1-2*sin(roll/2)^2 == cos(roll)
static float2 swivelGrad(float3x3 rot, float3 axis, float3 wt) {
	float3x3 crossM, dotM;
	crossM.c0 = cross(axis, rot.c0);
	crossM.c1 = cross(axis, rot.c1);
	crossM.c2 = cross(axis, rot.c2);
	dotM.c0   = dot(axis, rot.c0)*axis;
	dotM.c1   = dot(axis, rot.c1)*axis;
	dotM.c2   = dot(axis, rot.c2)*axis;
	// {x^2, y^2, z^2} == 0.5*diag.xyz - dot(0.25, diag.xyz) + 0.25
	float3 co = float3(rot[0][0]-dotM[0][0], rot[1][1]-dotM[1][1], rot[2][2]-dotM[2][2]);
	float3 si = float3(crossM[0][0], crossM[1][1], crossM[2][2]);
	float3 on = float3(dotM[0][0], dotM[1][1], dotM[2][2]);
	wt.z /= 0.5f + 0.5f * on.z; // 1-x^2-y^2
	wt = dot(0.5f, wt) - wt;
	return float2(dot(wt, co), dot(wt, si));
}
// assuming bendAxis == +X, bendGoal == +Y, boneAxis == +Z
// solve rot0 * (len0*boneAxis + rot1 * (len1*boneAxis + rot2 * ...)) == len2*boneAxis + ...
static void solveTrigIK(float3 len, out float3x3 rot0, out float3x3 rot1, out float3x3 rot2) {
	float3 co = clamp((len.yzx*len.yzx + len.zxy*len.zxy - len.xyz*len.xyz) / (2*len.yzx*len.zxy), -1, 1);
	float3 si = sqrt(1-co*co); // law of cosines
	rot0 = float3x3(1, 0, 0,	0, +co[1], +si[1],	0, -si[1], +co[1]);
	rot1 = float3x3(1, 0, 0,	0, -co[2], -si[2],	0, +si[2], -co[2]);
	rot2 = float3x3(1, 0, 0,	0, +co[0], +si[0],	0, -si[0], +co[0]);
}
// solve rot0 * (len0*boneAxis + rot1 * (len1*boneAxis + rot2 * ...)) == pos + rot * ...
static void solveLimbIK(float2 len, float3 pos, float3x3 rot, float3 wt0, float4 wt2, out float3x3 rot0, out float3x3 rot1, out float3x3 rot2) {
	float3x3 r0, r1, r2;
	solveTrigIK(float3(len, length(pos)), out(r0), out(r1), out(r2));
	// compute rot0 & rot2 without swivel and roll distribution
	float3 axis0 = normalize(pos);
	float3 axis2 = mul(r2, float3(0,0,1));
	rot0 = fromToRotate(float3(0,0,1), axis0, r0);
	rot2 = mul(transpose(mul(rot0, r1)), rot);
	// swivel limb to minimize cost (see swivelGrad)
	float2 c = normalize(swivelGrad(rot0, axis0, wt0.xyz) * float2(1, -1) + swivelGrad(rot2, axis2, wt2.xyz));
	rot0 = axisAngleRotate(axis0, float2(c.x, -c.y), rot0);
	rot2 = axisAngleRotate(axis2, c, rot2);
	// distribute roll2 into roll1 and roll2 to minimize wt2.w * roll1^2 + roll2^2
	c = swivelGrad(rot2, float3(0,0,1), float3(0,0,1));
	c = normalize(c * wt2.w + float2(length(c), 0));
	rot1 = mul(r1, float3x3(c.x,+c.y,0, -c.y,c.x,0, 0,0,1));
	rot2 = mul(float3x3(c.x,-c.y,0, +c.y,c.x,0, 0,0,1), rot2);
}
// compute cost weights so that optimal rotation angles are roughly proportional to limits
// wt0 == weightsFromLimits(x0, y0, z0, zp)
// wt2 == weightsFromLimits(x2, y2, z2, z1)
// static float4 weightsFromLimits(float4 limits) {
// 	float3 wt = 1/float3(limits.xy, dot(limits.zw,1));
// 	return float4(wt, limits.w/limits.z);
// }