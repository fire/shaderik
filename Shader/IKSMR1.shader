Shader "IK/IKSMR1" {
Properties {
	[Header(Texture)]
	[NoScaleOffset]
	_MainTex ("Albedo", 2D) = "white" {}
	_Color ("Color", Color) = (1,1,1,1)

	[Header(Clipping)]
	[Enum(UnityEngine.Rendering.CullMode)] _Cull("Face Culling", Float) = 2
	[Toggle(_ALPHATEST_ON)] _AlphaTest("Alpha Test", Float) = 0
	_Cutoff ("Alpha Cutoff", Range(0, 1)) = 0
	_NearClip ("NearClip", Float) = 0

	[Header(Motion Distribution (Bend.Swing.Twist))]
	_Spread00 ("Spine", 	Vector) = (40, 40, 40, 1)
	_Spread01 ("Head",		Vector) = (40, 40, 80, 1)
	_Spread10 ("UpperArm",	Vector)	= (90, 90, 45, 1)
	_Spread11 ("Hand",		Vector) = (30, 60,135, 1)
	_Spread20 ("UpperLeg",	Vector) = (60, 60, 60, 1)
	_Spread21 ("Foot",		Vector) = (30, 15, 30, 1)
	_Spread30 ("Eye",		Vector) = (60, 60,  9, 0)
	_Spread40 ("ThumbProximal",	Vector) = (20, 25,  9, 1)
	_Spread41 ("ThumbDistal",	Vector) = (40,  9,  9, 1)
	_Spread50 ("IndexProximal",	Vector) = (45, 20,  9, 1)
	_Spread51 ("IndexDistal",	Vector) = (45,  9,  9, 1)
}
SubShader {
	Tags { "Queue"="Geometry" "RenderType"="Opaque" }
	Pass {
		Tags { "LightMode"="ForwardBase" }
		Cull [_Cull]
		CGPROGRAM
		#pragma target 5.0
		#pragma exclude_renderers gles
		#pragma vertex vert
		#pragma fragment frag
		#pragma shader_feature _ALPHATEST_ON
		#include <UnityCG.cginc>
		#include <Lighting.cginc>
		#include "IKSMR.hlsl"
		#include "Frag.hlsl"

		void vert(VertInputIKSMR i, out FragInput o) {
			float3 vertex = i.vertex;
			float3 normal = i.normal;
			if(i.GetLimbRawId() >= 0) {
				packed3x4 mats[4] = (packed3x4[4])0;
				float3x4 matP = float3x4(1,0,0,0, 0,1,0,0, 0,0,1,0) * length(i.GetLimbTarget().c0);
				float3x4 mat[3];
				pack3x4(mats[0], matP);
				solveIK(i, matP, mat[0], mat[1], mat[2]);
				pack3x4(mats[1], mat[0]);
				pack3x4(mats[2], mat[1]);
				pack3x4(mats[3], mat[2]);

				vertex = normal = 0;
				UNITY_UNROLL for(uint J=0; J<QUALITY; J++) {
					uint bone; float4 vtx; float3 nml;
					i.GetSkin(J, bone, vtx, nml);
					float3x4 mat = unpack3x4(mats[bone]);
					vertex += mul(mat, vtx);
					normal += mul(mat, nml);
				}
			}
			UNITY_SETUP_INSTANCE_ID(i);
			UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
			o.vertex = mul(unity_ObjectToWorld, float4(vertex, 1));
			o.normal = mul(unity_ObjectToWorld, float4(normal, 0));
			o.pos = UnityWorldToClipPos(o.vertex);
			o.tex = refSpread()?i.GetUV():0;
		}
		ENDCG
	}
}
}