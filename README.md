# ShaderIK

ShaderIK implements a hacky fullbody humanoid IK based on 6 analytic limb IKs in HLSL. This project also contains a Unity editor script to turn a humanoid skinned avatar mesh into a skinned mesh followed by 6 limb targets.